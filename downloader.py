import argparse
import sys

from concurrent.futures import ThreadPoolExecutor
import requests
from requests_futures.sessions import FuturesSession
import time


def background_callback(sess, resp):
    # parse the json storing the result on the response object
    if resp.status_code == requests.codes.ok:
        resp.data = resp.json()
    else:
        resp.data = None


class JSONRetriever(object):
    def __init__(self, debug=False):
        self._executor = ThreadPoolExecutor(max_workers=10)
        self._session = FuturesSession(executor=self._executor)
        self._pending = {}
        self._results = []
        self._size = 0
        self.headers = {"Accept-Encoding": "gzip, deflate",}
        self.debug = debug

    def fetch(self, url):
        print "Fetching url %s" % url
        future = self._session.get(url, background_callback=background_callback, headers=self.headers)
        self._pending[future] = url

    def drain(self):
        # Look for completed requests by hand because in the real code
        # the responses my trigger further URLs to be retrieved so
        # self._pending is modified. New requests being added really
        # confused as_completed().
        for future in [f for f in self._pending if f.done()]:
            url = self._pending[future]
            del self._pending[future]

            response = future.result()
            if response.status_code == requests.codes.ok:
                self._results.append(response.data)
                self._size += len(response.content)
            else:
                self.fetch(url)

    def finish(self):
        while self._pending:
            self.drain()
            if self._pending:
                self.sleep(1)
            if self._size > 2000*1000:
                self.sleep(20, "API limitation")
                self._size = 0

    def stop(self):
        for i in self._pending:
            try:
                i.cancel()
            except Exception as e:
                sys.stderr.write("Caught: " + str(e) + "\n")

        self._executor.shutdown()

    def sleep(self, i, reason=None):
        msg = "Sleep for %s seconds" % i
        if reason:
            msg += ' because of %s' % reason
        print msg
        time.sleep(i)

    @classmethod
    def process(cls, urls, delay):
        retriever = cls()
        try:
            for url in urls:
                retriever.fetch(url)
                if delay > 0:  # may need a delay to rate limit requests
                    time.sleep(delay)
                    retriever.drain()  # clear any requests that completed while asleep

            retriever.finish()
        except KeyboardInterrupt:
            retriever.stop()
        else:
            return retriever._results


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Perform all REST calls")
    parser.add_argument("--delay", type=int, default=0)
    args = parser.parse_args()

    urls = [
        "http://webservices.nextbus.com/service/publicJSONFeed?command=routeConfig&a=ttc&r=5",
        "http://webservices.nextbus.com/service/publicJSONFeed?command=routeConfig&a=ttc&r=6",
        "http://webservices.nextbus.com/service/publicJSONFeed?command=routeConfig&a=ttc&r=7",
        "http://webservices.nextbus.com/service/publicJSONFeed?command=routeConfig&a=ttc&r=8",
        "http://webservices.nextbus.com/service/publicJSONFeed?command=routeConfig&a=ttc&r=9",
        "http://webservices.nextbus.com/service/publicJSONFeed?command=routeConfig&a=ttc&r=10",
        "http://webservices.nextbus.com/service/publicJSONFeed?command=routeConfig&a=ttc&r=11",
    ]
    print JSONRetriever.process(urls, args.delay)
