import json
import time
from argparse import ArgumentParser
from collections import defaultdict

import polyline
import requests

from downloader import JSONRetriever

API_URL = (
    "http://webservices.nextbus.com/service/publicJSONFeed?command={command}"
)


def load_arguments():
    """Load user's arguments from command line"""
    parser = ArgumentParser(description="nextbus agency downloader")
    parser.add_argument(
        "-a",
        dest="agency",
        required=True,
        help="agency tag",
    )
    parser.add_argument(
        "-o",
        dest="output",
        required=True,
        help="output json file path where to save results",
    )
    args = parser.parse_args()
    return args


def get_api_url(command, **kwargs):
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:20.0) Gecko/20100101 Firefox/20.0",
        "Accept-Encoding": "gzip, deflate",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
        "Accept-Language": "en-US,en;q=0.5",
        "Connection": "keep-alive",
    }
    url = build_url(command, **kwargs)
    resp = requests.get(url, headers=headers)
    resp.raise_for_status()
    return resp.json()


def build_url(command, **kwargs):
    replaced_keys = {
        'agency': 'a',
        'route': 'r',
    }
    full_url = API_URL.format(command=command)
    if kwargs:
        url_kwargs = ["%s=%s" % (replaced_keys.get(k, k), v) for k, v in kwargs.items()]
        full_url += "&" + "&".join(url_kwargs)
    return full_url


def check_agency(agency):
    """Check if the name of agency is correct (ie exists in NextBusXMLFeed"""
    print "Checking agency name: %s" % agency
    agency_dict = get_api_url('agencyList')
    agency_list = [a["tag"] for a in agency_dict['agency']]
    agency_correct = agency in agency_list
    print "Agency name %s" % "correct" if agency_correct else "incorrect"
    return agency_correct


def get_routes_list(agency):
    print "Getting list of routes"
    route_dict = get_api_url('routeList', agency=agency)
    routes = route_dict['route']
    if isinstance(routes, list):
        route_list = [a["tag"] for a in route_dict['route']]
    else:
        route_list = [routes["tag"]]
    print "Found %s routes for %s" % (len(route_list), agency)
    return route_list


def get_routes_details(agency, routes_list):
    print "Downloading details for each route"
    urls = [build_url('routeConfig', route=route, agency=agency) for route in routes_list]
    routes = JSONRetriever.process(urls, delay=0.5)
    return routes


def save_json_data_to_file(json_data, file_path):
    print "Saving output result to %s" % file_path
    with open(file_path, "w") as file_obj:
        file_obj.write(json.dumps(json_data, indent=4))


def analyze_multi_route_stops(routes_details):
    """ find MultiRouteStop """
    multi_route_stops = defaultdict(int)
    for rd in routes_details:
        for stop in rd["route"]["stop"]:
            stop_code = stop.get("stopId")
            if stop_code:
                multi_route_stops[stop_code] += 1
    return multi_route_stops


def prepare_result(routes_details):
    """prepare result to required format"""
    multi_route_stops = analyze_multi_route_stops(routes_details)

    actual_routes = []
    for i, rd in enumerate(routes_details):
        route_data = rd["route"]
        color = route_data.pop("color")
        color_hex = ("#%s" % color).upper()

        stops_dict = {stop["tag"]: stop for stop in route_data.pop("stop", [])}

        actual_paths = []
        paths = route_data.pop('path', [])
        for path in paths:
            points = path["point"]
            raw_points = []
            for point in points:
                lon, lat = float(point.pop('lon')), float(point.pop('lat'))
                raw_points.append((lat, lon))

            actual_paths.append({
                "colorHex": color_hex,
                "pointString": polyline.encode(raw_points),
            })

        directions = route_data.pop("direction", [])
        for direction in directions:
            direction.pop('useForUI')
            direction_stops = direction.pop("stop", [])
            direction_branch = direction.pop("branch", None)
            for stop in direction_stops:
                stop_tag = stop["tag"]
                stop_info = stops_dict[stop_tag]
                stop.update(stop_info)
                stop["direction"] = direction["name"]
                stop["name"] = stop.pop("title")
                if "stopId" in stop:
                    stop["code"] = stop.pop("stopId")
                    stop["isMultiRouteStop"] = multi_route_stops[stop["code"]] > 1
                else:
                    stop["isMultiRouteStop"] = False

                stop["latitude"] = float(stop.pop("lat"))
                stop["longitude"] = float(stop.pop("lon"))
            direction['stops'] = direction_stops

        route_data["directions"] = directions
        route_data["paths"] = actual_paths
        route_data["shortName"] = route_data.pop("tag")
        route_data["tag"] = route_data["shortName"]
        route_data["colorHex"] = color_hex
        route_data["orderNumber"] = i
        route_data["lonMax"] = float(route_data.pop("lonMax"))
        route_data["lonMin"] = float(route_data.pop("lonMin"))
        route_data["latMax"] = float(route_data.pop("latMax"))
        route_data["latMin"] = float(route_data.pop("latMin"))
        route_data["longName"] = route_data["title"].split("-", 1)[1]
        route_data.pop("oppositeColor")

        actual_routes.append(route_data)

    result = {
        "version": 1,
        "lastUpdatedTimestamp": time.time(),
        "routes": actual_routes
    }
    return result


def process():
    """Main function"""
    args = load_arguments()
    agency = args.agency

    if not check_agency(agency):
        raise ValueError('Agency name is incorect: %s' % agency)

    routes_list = get_routes_list(agency)
    # routes_list = routes_list[1:2]
    routes_details = get_routes_details(agency, routes_list)
    result_json = prepare_result(routes_details)

    save_json_data_to_file(result_json, args.output)


if __name__ == "__main__":
    process()
