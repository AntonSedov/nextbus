requires Python 2.7

script to parse NEXTBUS API, download the info and output as a json file in a specific format

Usage

output to file (with overwrite)
python download_agency_data.py -a=atlanta-sc -o="output22.json"

Expected output

{
  "version" : 1,
  "lastUpdatedTimestamp" : 1539303647.680917,
  "routes" : [
        { *route_info
          "directions" : [
                {
                  "title" : "North - 8 Broadview towards Coxwell",
                  "name" : "North",
                  "tag" : "8_0_8",
                  "stops": [**list of dicts with stops**], +
                },
          }]
          "paths" : [**list of dicts with paths**],
        }
}
